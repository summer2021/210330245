#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(unused_mut)]
#![allow(unused_assignments)]

use crate::{classic::{
    execute::Executor,
    message::{
        Command, CommandLeaderBookKeeping, CoreInfo, Instance, InstanceEntry, LogEntry, Operation,
        PreAcceptReplyPayload, PrepareReplyPayload, ReplicaId, State,
    },
    server::EpaxosServerInner,
}, cli::cli::CLIParam};

use super::config::REPLICAS_NUM;
use log::info;
use std::{
    cmp,
    collections::{BTreeMap, HashMap, HashSet},
    fmt,
    ops::{Shl, Shr},
};

pub struct EpaxosLogic {
    //pub id: ReplicaId,
    pub cmds: Vec<HashMap<usize, LogEntry>>,
    //pub instance_number: u32,
    // instance -> ballot
    //pub inst_ballot: BTreeMap<Instance, u32>,
    pub exec: Executor,
    pub info: CoreInfo,
    pub instance_entry_space: BTreeMap<Instance, InstanceEntry>,
    pub current_instance: Vec<u32>,
    pub commited_upto_instance: Vec<u32>,
    pub execed_upto_instance: Vec<u32>,
    pub conflicts: Vec<BTreeMap<String, u32>>,
    pub max_seq_per_key: BTreeMap<String, u32>,
    pub max_seq: u32,
    pub lastest_cp_replica: u32,
    pub lastest_cp_instance: u32,
    pub instance_to_revovery: Vec<Instance>,
}

impl EpaxosLogic {
    // id int, peerAddrList []string, thrifty bool, exec bool, dreply bool, beacon bool, durable bool
    pub fn init(
        param: CLIParam
    ) -> EpaxosLogic {
        let commands = vec![HashMap::new(); REPLICAS_NUM];
        let info = CoreInfo {
            n: REPLICAS_NUM,
            id: param.replica_id,
            //peer_addr_list: param.peer_addr_list,
            peers: Vec::new(),
            peer_readers: Vec::new(),
            peer_writers: Vec::new(),
            alive: BTreeMap::new(),
            state: None,
            shutdown: false,
            thrifty: param.thrifty,
            beacon: param.beacon,
            durable: param.durable,
            stable_store: None,
            preferred_peer_order: Vec::new(),
            ewma: Vec::new(),
            on_client_connect: false,
            dreply: param.dreply,
            exec: param.exec,
            defer_map: BTreeMap::new(),
            tpa_payload: None,
        };
        // spawn run to start the core logic

        EpaxosLogic {
            cmds: commands,
            //instance_number: 0,
            exec: Executor::default(),
            //inst_ballot: BTreeMap::new(),
            info,
            current_instance: Vec::new(),
            commited_upto_instance: Vec::new(),
            execed_upto_instance: Vec::new(),
            conflicts: Vec::new(),
            max_seq_per_key: BTreeMap::new(),
            max_seq: 0,
            lastest_cp_replica: 0,
            lastest_cp_instance: 0,
            instance_to_revovery: Vec::new(),
            instance_entry_space: BTreeMap::new(),
        }
    }

    pub fn _execute(&mut self, instance: &Instance) {
        let mut gr_map = Vec::new();
        let mut seq_slot = BTreeMap::new();
        self.exec
            .build_graph(instance.slot, &mut gr_map, &mut seq_slot);

        // Construct the slot -> Graph
        let mut bs = BTreeMap::new();
        let mut deps = Vec::new();
        for dep in self.cmds[instance.replica as usize]
            .get(&(instance.slot as usize))
            .unwrap()
            .deps
            .iter()
        {
            deps.push(*dep as usize);
        }

        bs.insert(instance.slot as usize, deps);
        self.exec.graph = gr_map;
        self.exec.vertices = bs;
        self.exec.seq_slot = seq_slot;
        //self.exec.cmds = self.cmds[instance.replica as usize].clone();

        // execute
        self.exec.execute()
    }

    pub fn find_pre_accept_conflicts(
        &mut self,
        cmds: &Vec<Command>,
        instance: Instance,
        seq: u32,
        deps: &Vec<u32>,
    ) -> (bool, u32, u32) {
        if let Some(inst) = self.instance_entry_space.get(&instance) {
            if inst.command.as_ref().unwrap().len() > 0 {
                if inst.state.unwrap() >= State::Accepted {
                    // already ACCEPTED or COMMITTED
                    // we consider this a conflict because we shouldn't regress to PRE-ACCEPTED
                    return (true, instance.replica, instance.slot)
                }
                if inst.seq == self.info.tpa_payload.as_ref().unwrap().seq && equal(inst.deps.clone(), self.info.tpa_payload.as_ref().unwrap().deps.to_vec()) {
                    // already PRE-ACCEPTED, no point looking for conflicts again
                    return (false, instance.replica, instance.slot);
                }
            }
        }

        for q in 0..self.info.n {
            for i in self.execed_upto_instance[q]..self.current_instance[q] {
                if instance.replica == q as u32 && instance.slot == i {
                    // no point checking past instance in replica's row, since replica would have
                    // set the dependencies correctly for anything started after instance
                    break;
                }

                if i == deps[q] {
                    // the instance cannot be a dependency for itself
                    continue;
                }

                if let Some(inst) = self.instance_entry_space.get(&Instance {
                        replica: q as u32,
                        slot: i,
                }) {
                    if inst.command.is_none() || inst.command.as_ref().unwrap().len() == 0 {
                        continue;
                    }
                    if inst.deps[instance.replica as usize] > instance.slot {
                        continue;
                    }

                    if self.conflict_batch(inst.command.as_ref().unwrap(), cmds) {
                        if i > deps[q]
                            || (i < deps[q]
                                && inst.seq >= seq
                                && (q != instance.replica as usize
                                    || inst.state.unwrap() > State::PreAcceptedEq))
                        {
                            // this is a conflict
                            return (true, q as u32, i);
                        }
                    }
                } else {
                    continue;
                } 
            }
        }
        return (false, u32::MAX, u32::MAX);
    }

    fn conflict(&self, cmd1: &Command, cmd2: &Command) -> bool {
        if cmd1.key == cmd2.key {
            if cmd1.op == Operation::Put || cmd2.op == Operation::Put {
                return true;
            }
        }
        return false;
    }

    fn conflict_batch(&self, batch1: &Vec<Command>, batch2: &Vec<Command>) -> bool {
        for i in 0..batch1.len() {
            for j in 0..batch2.len() {
                if self.conflict(&batch1[i], &batch2[j]) {
                    return true;
                }
            }
        }
        return false;
    }

    pub fn clear_hashtables(&mut self) {
        for q in 0..self.info.n {
            // XXX: does it satisfied??
            self.conflicts[q].clear();
        }
    }

    pub fn update_attributes(
        &mut self,
        cmds: Vec<Command>,
        mut seq: u32,
        mut deps: Vec<u32>,
        instance: &Instance,
    ) -> (u32, Vec<u32>, bool) {
        let mut changed = false;
        for q in 0..self.info.n {
            if self.info.id != instance.replica && q == instance.replica as usize {
                continue;
            }

            for i in 0..cmds.len() {
                let present = self.conflicts[q].get(&cmds[i].key);
                match present {
                    Some(d) => {
                        if d > &deps[q] {
                            deps[q] = *d;
                            if seq
                                <= self
                                    .instance_entry_space
                                    .get(&Instance {
                                        replica: q as u32,
                                        slot: i as u32,
                                    })
                                    .unwrap()
                                    .seq
                            {
                                seq = self
                                    .instance_entry_space
                                    .get(&Instance {
                                        replica: q as u32,
                                        slot: i as u32,
                                    })
                                    .unwrap()
                                    .seq;
                            }
                            changed = true;
                            break;
                        }
                    }
                    None => {
                        unreachable!();
                    }
                }
            }
        }

        for i in 0..cmds.len() {
            let present = self.max_seq_per_key.get(&cmds[i].key);
            match present {
                Some(s) => {
                    if seq <= *s {
                        changed = true;
                        seq = s + 1;
                    }
                }
                None => {
                    unreachable!();
                }
            }
        }

        return (seq, deps, changed);
    }

    pub fn update_conflicts(&mut self, cmds: &Vec<Command>, instance: &Instance, seq: u32) {
        for i in 0..cmds.len() {
            let present = self.conflicts[instance.replica as usize].get(&cmds[i].key);
            match present {
                Some(d) => {
                    if d < &instance.slot {
                        self.conflicts[instance.replica as usize]
                            .insert(cmds[i].key.clone(), instance.slot);
                    }
                }
                None => {
                    self.conflicts[instance.replica as usize].insert(cmds[i].key.clone(), instance.slot);
                }
            }

            let present = self.max_seq_per_key.get(&cmds[i].key);
            match present {
                Some(s) => {
                    if s < &seq {
                        self.max_seq_per_key.insert(cmds[i].key.clone(), seq);
                    }
                }
                None => {
                    self.max_seq_per_key.insert(cmds[i].key.clone(), seq);
                }
            }
        }
    }

    pub fn record_payload_metadata(&self, instance: &InstanceEntry) {}

    pub fn record_commands(&self, cmds: &Vec<Command>) {}

    pub fn sync(&self) {}

    pub fn update_committed(&mut self, instance: &Instance) {
        let iter_inst = Instance {
            replica: instance.replica,
            slot: self.commited_upto_instance[instance.replica as usize] + 1,
        };
        let iter_inst_entry = self.instance_entry_space.get(&iter_inst);
        while !iter_inst_entry.is_none()
            && (iter_inst_entry.unwrap().state.unwrap() == State::Committed
                || iter_inst_entry.unwrap().state.unwrap() == State::Executed)
        {
            self.commited_upto_instance[instance.replica as usize] += 1;
        }
    }

    pub fn merge_attributes(
        &self,
        mut seq1: u32,
        mut deps1: Vec<u32>,
        seq2: u32,
        deps2: Vec<u32>,
    ) -> (u32, Vec<u32>, bool) {
        let mut equal = true;
        if seq1 != seq2 {
            equal = false;
            if seq2 > seq1 {
                seq1 = seq2;
            }
        }

        for q in 0..self.info.n {
            if q == self.info.id as usize {
                continue;
            }

            if deps1[q] != deps2[q] {
                equal = false;
                if deps2[q] > deps1[q] {
                    deps1[q] = deps2[q];
                }
            }
        }

        return (seq1, deps1, equal);
    }

    pub fn is_initial_ballot(&self, ballot: u32) -> bool {
        ballot >> 4 == 0
    }

    pub fn update_deferred(&mut self, dr: u32, di: u32, r: u32, i: u32) {
        let daux = (dr as u64).shl(32) | (di as u64);
        let aux = (r as u64).shl(32) | (i as u64);
        self.info.defer_map.insert(aux, daux);
    }

    pub fn deferred_by_instance(&self, q: u32, i: u32) -> (bool, u32, u32) {
        let mut aux: u64 = (q as u64).shl(32) | (i as u64);
        let daux = self.info.defer_map.get(&aux);
        let mut dq: u32 = 0;
        let mut di: u32 = 0;
        match daux {
            Some(dd) => {
                dq = (*dd as u32).shr(32);
                di = *dd as u32;
            }
            None => {
                return (false, 0, 0);
            }
        }
        return (true, dq, di);
    }

    // Ballot helper function
    pub fn make_unique_ballot(&self, ballot: u32) -> u32 {
        return (ballot << 4) | self.info.id;
    }

    pub fn make_ballot_larger_than(&self, ballot: u32) -> u32 {
        return self.make_unique_ballot((ballot >> 4) + 1);
    }
}

pub fn equal(a: Vec<u32>, b: Vec<u32>) -> bool {
    if a.len() != b.len() {
        return false;
    }
    for i in 0..a.len() {
        if a[i] != b[i] {
            return false;
        }
    }
    return true;
}

impl fmt::Debug for LogEntry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(
            f,
            "\nWrite(key = {}, value = {})\nSeq = {}\nDeps = {:#?}\nState = {:?}\n",
            self.key, self.value, self.seq, self.deps, self.state
        )
    }
}

impl fmt::Debug for Instance {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "(Replica: {}, Slot: {})", self.replica, self.slot)
    }
}

impl fmt::Debug for State {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            State::PreAccepted => write!(f, "PreAccepted"),
            State::Accepted => write!(f, "Accepted"),
            State::Committed => write!(f, "Committed"),
            State::Executed => write!(f, "Executed"),
            State::PreAcceptedEq => write!(f, "PreAcceptedEQ"),
        }
    }
}
