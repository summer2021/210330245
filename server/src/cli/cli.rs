#![allow(unused_imports)]

use std::env;

use smol::fs::File;
// use grpcio::Server;
use yaml_rust::{YamlLoader, YamlEmitter};
use smol::io::AsyncReadExt;

#[derive(Default, Clone, Debug)]
pub struct CLIParam {
    pub replica_id: u32,
    //pub peer_addr_list: Vec<String>,
    pub thrifty: bool,
    pub exec: bool,
    pub dreply: bool,
    pub beacon: bool,
    pub durable: bool,
    pub addr: String,
    pub port: u16,
    pub listen: u16,
}



impl CLIParam {
    pub fn get_param(&self) -> Option<Self> {
        let args: Vec<String> = env::args().collect();
        let id: u32 = args[1].parse().unwrap();
        let master_addr: String = args[2].parse().unwrap();
        let master_port: u16 = args[3].parse().unwrap();
        let port_num: u16 = args[4].parse().unwrap();
        let thrifty: bool = args[5].parse().unwrap();
        let exec: bool = args[6].parse().unwrap();
        let dreply: bool = args[7].parse().unwrap();
        let beacon: bool = args[8].parse().unwrap();
        let durable: bool = args[9].parse().unwrap();

        //let (replica_id, node_list) = register_with_command_leader(master_addr.to_string(), master_port);

        Some(CLIParam {
            replica_id: id,
            addr: master_addr,
            port: master_port,
            listen: port_num,
            //peer_addr_list: node_list.to_vec(),
            thrifty,
            exec,
            dreply,
            beacon,
            durable,
        })
    }

    pub async fn load_file(&self, filepath: String) -> Self {
        let file = File::open(filepath).await;
        match file {
            Ok(mut f) => {
                let mut contents = Vec::new();
                let result = f.read_to_end(&mut contents).await;
                if result.is_err() {
                    panic!("Read File is wrong!");
                }
                let param_str = std::str::from_utf8(&contents).unwrap();
                let docs = YamlLoader::load_from_str(param_str);
                if docs.is_err() {
                    panic!("Convert String to Yaml is wrong!!!");
                }
                CLIParam {
                    replica_id: docs.as_ref().unwrap()[0]["id"].as_i64().unwrap() as u32,
                    thrifty: docs.as_ref().unwrap()[0]["thrifty"].as_bool().unwrap(),
                    exec: docs.as_ref().unwrap()[0]["exec"].as_bool().unwrap(),
                    dreply: docs.as_ref().unwrap()[0]["dreply"].as_bool().unwrap(),
                    beacon: docs.as_ref().unwrap()[0]["beacon"].as_bool().unwrap(),
                    durable: docs.as_ref().unwrap()[0]["durable"].as_bool().unwrap(),
                    addr: docs.as_ref().unwrap()[0]["master_addr"].as_str().unwrap().to_string(),
                    port: docs.as_ref().unwrap()[0]["master_port"].as_i64().unwrap() as u16,
                    listen: docs.as_ref().unwrap()[0]["port_num"].as_i64().unwrap() as u16,
                }
            },
            Err(e) => {
                panic!("File is something wrong{:?}", e);
            },
        }
    }
}

// pub fn register_with_command_leader(addr: String, port: u16) -> (u32, &'static Vec<String>) {
//     let mut args = RegisterArgs::new(addr, port);
//     let mut reply = RegisterReply::new();

    
// }

#[cfg(test)]
mod test {
    use super::CLIParam;

    #[test]
    fn test_cli() {
        let ccc = CLIParam::default();
        smol::block_on(async {
            let res = ccc.load_file("/Users/blade/Documents/Repos/epaxos/doc/example_epaxos.yml".to_string()).await;
            println!("ddhdhdhhdh{:?}", res);
        });
    }
}
