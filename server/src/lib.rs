#![feature(destructuring_assignment)]
pub mod cli;
pub mod classic;

#[macro_use]
extern crate lazy_static;